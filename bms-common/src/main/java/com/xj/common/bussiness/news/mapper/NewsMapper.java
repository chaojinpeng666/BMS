package com.xj.common.bussiness.news.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xj.common.bussiness.news.entity.News;

/**
 * <p>
 * 栏目管理 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2022-07-26
 */
public interface NewsMapper extends BaseMapper<News> {

}

package com.xj.admin.bussiness.news.web;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.feilong.core.Validator;
import com.feilong.core.bean.ConvertUtil;
import com.xj.admin.base.index.web.BaseController;
import com.xj.admin.bussiness.news.service.INewsService;
import com.xj.admin.util.dtgrid.model.Pager;
import com.xj.common.base.common.bean.AbstractBean;
import com.xj.common.base.common.exception.EnumSvrResult;
import com.xj.common.base.common.util.JsonUtil;
import com.xj.common.bussiness.news.entity.News;

/**
 * <p>
 * 栏目管理  前端控制器
 * </p>
 *
 * @author xj
 * @since 2017-01-05
 */
@Controller
@RequestMapping("/news/")

public class TbNewsController extends BaseController {

	@Autowired
	private INewsService newsService;
	
	@GetMapping("listUI")
    public String listUI() {
		return "news/list";
    }
	
	@PostMapping("list")
	@ResponseBody
    public Object list(String gridPager) {
		Pager pager = JsonUtil.getObjectFromJson(gridPager, Pager.class);
		Map<String, Object> parameters = null;
		if(Validator.isNullOrEmpty(pager.getParameters())){
			parameters = new HashMap<>();
		}else{
			parameters = pager.getParameters();
		}
		Page<News> list = newsService.page(new Page<News>(pager.getNowPage(), pager.getPageSize()), new QueryWrapper<News>().allEq(parameters).orderByDesc("id"));
		makeGridResult(parameters, pager, list);
		return parameters;
    }
	
	@GetMapping("form")
    public String form(Map<String,Object> map) {
		return "news/form";
    }
	
	@PostMapping("save")
	@ResponseBody
	public AbstractBean add(News tbnews){
		AbstractBean bean = new AbstractBean();
			if(tbnews.getId()==null){
				tbnews.setCreateTime(LocalDateTime.now());
				tbnews.setCreateUser(getUserEntity().getAccountName());
				tbnews.setUpdateTime(LocalDateTime.now());
			}else{
				tbnews.setUpdateTime(LocalDateTime.now());
				tbnews.setUpdateUser(getUserEntity().getAccountName());
			}
			tbnews.setIsShow(tbnews.getIsShow()==null?2:1);
			if(!newsService.saveOrUpdate(tbnews)){
				bean.setStatus(EnumSvrResult.ERROR.getVal());
				bean.setMessage(EnumSvrResult.ERROR.getName());
			}
		return bean;
	}
	
	@DeleteMapping("{id}/delete")
	@ResponseBody
    public AbstractBean delete(@PathVariable(required=true) Integer id) {	
		if(!newsService.removeById(id)){
			return fail(EnumSvrResult.ERROR);
		}
		return success();
    }	
	
	@GetMapping("{id}/select")
    public String select(Map<String,Object> map,@PathVariable(required=true) Integer id) {	
		News tbnews = newsService.getById(id);
		map.put("record", tbnews);
		return "news/edit";
    }	
	
	@DeleteMapping("{ids}/deleteBatch")
	@ResponseBody
	public AbstractBean deleteBatch(@PathVariable(required=true) String ids) {
		if(!newsService.removeBatchByIds(ConvertUtil.toList(ids.split(",")))){
			return fail(EnumSvrResult.ERROR);
		}
		return success();
	}
}

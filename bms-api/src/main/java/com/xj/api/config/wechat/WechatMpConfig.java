package com.xj.api.config.wechat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.config.WxMpConfigStorage;
import me.chanjar.weixin.mp.config.impl.WxMpDefaultConfigImpl;

/**
 * 
 * @author Eric.Xu
 * 微信公众号初始化配置
 */
@Configuration
@ConditionalOnClass(WxMpService.class)
@EnableConfigurationProperties(WechatMpAccountConfig.class)
public class WechatMpConfig{
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private WechatMpAccountConfig wechatAccountConfig;
	
	@Bean
	public WxMpService wxMpService() {
		WxMpService wxMpService = new WxMpServiceImpl();
		wxMpService.setWxMpConfigStorage(wxMpConfigStorage());
		return wxMpService;
	}
	
	@Bean
	public WxMpConfigStorage wxMpConfigStorage() {
		logger.info("----------init WxMpService------------");
		WxMpDefaultConfigImpl config = new WxMpDefaultConfigImpl();
		config.setAppId(wechatAccountConfig.getAppId());
		config.setSecret(wechatAccountConfig.getAppSecret());
		return config;
	}


	
}
